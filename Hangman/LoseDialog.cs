﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hangman
{
    public partial class LoseDialog : Form
    {
        public LoseDialog()
        {
            InitializeComponent();
        }

        public LoseDialog(Form1 g, string w)
        {
            InitializeComponent();
            label2.Text = w;
            game = g;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            game.restartGame();
            Close();
        }
    }
}
