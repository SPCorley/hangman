﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace Hangman
{
    public partial class StartDialog : Form
    {
        public StartDialog()
        {
            InitializeComponent();
        }

        public StartDialog(Form1 g)
        {
            InitializeComponent();
            game = g;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void shortButton_Click(object sender, EventArgs e)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            StreamReader shortFile = new StreamReader(asm.GetManifestResourceStream("Hangman.WordFiles.short_words.txt"));
            parseFile(shortFile);

            Random r = new Random();
            int randIndex = r.Next(0, words.Count);
            game.startGame(words[randIndex]);

            Close();
        }

        private void mediumButton_Click(object sender, EventArgs e)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            StreamReader mediumFile = new StreamReader(asm.GetManifestResourceStream("Hangman.WordFiles.medium_words.txt"));
            parseFile(mediumFile);

            Random r = new Random();
            int randIndex = r.Next(0, words.Count);
            game.startGame(words[randIndex]);

            Close();
        }

        private void longButton_Click(object sender, EventArgs e)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            StreamReader longFile = new StreamReader(asm.GetManifestResourceStream("Hangman.WordFiles.long_words.txt"));
            parseFile(longFile);

            Random r = new Random();
            int randIndex = r.Next(0, words.Count);
            game.startGame(words[randIndex]);

            Close();
        }

        private void parseFile (System.IO.StreamReader file)
        {
            string word;
            while ((word = file.ReadLine()) != null)
            {
                words.Add(word);
            }
            file.Close();
        }
    }
}
