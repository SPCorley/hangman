﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Hangman.Properties;

namespace Hangman
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            guessedLetters = new List<char>();
            wrongGuesses = 0;

            letterButtons = new Dictionary<object, char>();
            letterButtons.Add(aButton, 'A');
            letterButtons.Add(bButton, 'B');
            letterButtons.Add(cButton, 'C');
            letterButtons.Add(dButton, 'D');
            letterButtons.Add(eButton, 'E');
            letterButtons.Add(fButton, 'F');
            letterButtons.Add(gButton, 'G');
            letterButtons.Add(hButton, 'H');
            letterButtons.Add(iButton, 'I');
            letterButtons.Add(jButton, 'J');
            letterButtons.Add(kButton, 'K');
            letterButtons.Add(lButton, 'L');
            letterButtons.Add(mButton, 'M');
            letterButtons.Add(nButton, 'N');
            letterButtons.Add(oButton, 'O');
            letterButtons.Add(pButton, 'P');
            letterButtons.Add(qButton, 'Q');
            letterButtons.Add(rButton, 'R');
            letterButtons.Add(sButton, 'S');
            letterButtons.Add(tButton, 'T');
            letterButtons.Add(uButton, 'U');
            letterButtons.Add(vButton, 'V');
            letterButtons.Add(wButton, 'W');
            letterButtons.Add(xButton, 'X');
            letterButtons.Add(yButton, 'Y');
            letterButtons.Add(zButton, 'Z');
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            StartDialog startDialog = new StartDialog(this);
            startDialog.ShowDialog();
        }

        public void startGame(string word)
        {
            startButton.Enabled = false;
            chosenWord = word.ToUpper();

            System.Windows.Forms.Control.ControlCollection letters = flowLayoutPanel1.Controls;
            for (int i = 0; i < chosenWord.Length; i++)
            {
                letters[i].Visible = true;
            }

            System.Windows.Forms.Control.ControlCollection buttons = tableLayoutPanel1.Controls;
            foreach (Control button in buttons)
            {
                button.Enabled = true;
            }
            restartButton.Enabled = true;

            foreach (char letter in chosenWord)
            {
                guessedLetters.Add('?');
            }
        }

        public void restartGame()
        {
            System.Windows.Forms.Control.ControlCollection letters = flowLayoutPanel1.Controls;
            foreach (Control letter in letters)
            {
                letter.Visible = false;
                letter.Text = null;
            }
            guessedLetters.Clear();
            wrongGuesses = 0;
            changePicture();

            StartDialog startDialog = new StartDialog(this);
            startDialog.ShowDialog();
        }

        public void showLostDialog()
        {
            System.Windows.Forms.Control.ControlCollection buttons = tableLayoutPanel1.Controls;
            foreach (Control button in buttons)
            {
                button.Enabled = false;
            }
            LoseDialog loseDialog = new LoseDialog(this, chosenWord);
            loseDialog.ShowDialog();
        }

        public void showWonDialog()
        {
            System.Windows.Forms.Control.ControlCollection buttons = tableLayoutPanel1.Controls;
            foreach (Control button in buttons)
            {
                button.Enabled = false;
            }
            WinDialog winDialog = new WinDialog(this);
            winDialog.ShowDialog();
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void checkWord(char letter)
        {
            System.Windows.Forms.Control.ControlCollection letters = flowLayoutPanel1.Controls;
            for (int i = 0; i < chosenWord.Length; i++)
            {
                if (chosenWord.ToCharArray()[i] == letter)
                {
                    letters[i].Text = letter.ToString();
                    guessedLetters[i] = letter;
                }
            }

            if (!guessedLetters.Contains('?'))
            {
                showWonDialog();
            }
        }

        private void changePicture()
        {
            switch (wrongGuesses)
            {
                case 0:
                    pictureBox1.Image = Resources.Hangman;
                    break;
                case 1:
                    pictureBox1.Image = Resources.Hangman1;
                    break;
                case 2:
                    pictureBox1.Image = Resources.Hangman2;
                    break;
                case 3:
                    pictureBox1.Image = Resources.Hangman3;
                    break;
                case 4:
                    pictureBox1.Image = Resources.Hangman4;
                    break;
                case 5:
                    pictureBox1.Image = Resources.Hangman5;
                    break;
                case 6:
                    pictureBox1.Image = Resources.Hangman6;
                    showLostDialog();
                    break;
            }
        }

        private void letterButton_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            button.Enabled = false;

            char letter;
            letterButtons.TryGetValue(sender, out letter);
            if (chosenWord.Contains(letter))
            {
                checkWord(letter);
            }
            else
            {
                wrongGuesses++;
                changePicture();
            }

            restartButton.Focus();
        }

        private void restartButton_Click(object sender, EventArgs e)
        {
            restartGame();
        }
    }
}
