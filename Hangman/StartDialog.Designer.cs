﻿using System.Collections.Generic;

namespace Hangman
{
    partial class StartDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.shortButton = new System.Windows.Forms.Button();
            this.mediumButton = new System.Windows.Forms.Button();
            this.longButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(46, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose word length";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // shortButton
            // 
            this.shortButton.Location = new System.Drawing.Point(67, 86);
            this.shortButton.Name = "shortButton";
            this.shortButton.Size = new System.Drawing.Size(75, 23);
            this.shortButton.TabIndex = 1;
            this.shortButton.Text = "Short";
            this.shortButton.UseVisualStyleBackColor = true;
            this.shortButton.Click += new System.EventHandler(this.shortButton_Click);
            // 
            // mediumButton
            // 
            this.mediumButton.Location = new System.Drawing.Point(67, 128);
            this.mediumButton.Name = "mediumButton";
            this.mediumButton.Size = new System.Drawing.Size(75, 23);
            this.mediumButton.TabIndex = 2;
            this.mediumButton.Text = "Medium";
            this.mediumButton.UseVisualStyleBackColor = true;
            this.mediumButton.Click += new System.EventHandler(this.mediumButton_Click);
            // 
            // longButton
            // 
            this.longButton.Location = new System.Drawing.Point(67, 169);
            this.longButton.Name = "longButton";
            this.longButton.Size = new System.Drawing.Size(75, 23);
            this.longButton.TabIndex = 3;
            this.longButton.Text = "Long";
            this.longButton.UseVisualStyleBackColor = true;
            this.longButton.Click += new System.EventHandler(this.longButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(118, 241);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // StartDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(205, 276);
            this.ControlBox = false;
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.longButton);
            this.Controls.Add(this.mediumButton);
            this.Controls.Add(this.shortButton);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "StartDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Start";
            this.ResumeLayout(false);
            this.PerformLayout();

            words = new List<string>();
        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button shortButton;
        private System.Windows.Forms.Button mediumButton;
        private System.Windows.Forms.Button longButton;
        private System.Windows.Forms.Button cancelButton;

        private List<string> words;
        private Form1 game;
    }
}