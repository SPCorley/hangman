﻿using System.Collections.Generic;
namespace Hangman
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tButton = new System.Windows.Forms.Button();
            this.sButton = new System.Windows.Forms.Button();
            this.rButton = new System.Windows.Forms.Button();
            this.qButton = new System.Windows.Forms.Button();
            this.pButton = new System.Windows.Forms.Button();
            this.oButton = new System.Windows.Forms.Button();
            this.nButton = new System.Windows.Forms.Button();
            this.mButton = new System.Windows.Forms.Button();
            this.lButton = new System.Windows.Forms.Button();
            this.kButton = new System.Windows.Forms.Button();
            this.jButton = new System.Windows.Forms.Button();
            this.iButton = new System.Windows.Forms.Button();
            this.hButton = new System.Windows.Forms.Button();
            this.gButton = new System.Windows.Forms.Button();
            this.fButton = new System.Windows.Forms.Button();
            this.eButton = new System.Windows.Forms.Button();
            this.dButton = new System.Windows.Forms.Button();
            this.cButton = new System.Windows.Forms.Button();
            this.bButton = new System.Windows.Forms.Button();
            this.zButton = new System.Windows.Forms.Button();
            this.yButton = new System.Windows.Forms.Button();
            this.xButton = new System.Windows.Forms.Button();
            this.wButton = new System.Windows.Forms.Button();
            this.vButton = new System.Windows.Forms.Button();
            this.uButton = new System.Windows.Forms.Button();
            this.aButton = new System.Windows.Forms.Button();
            this.startButton = new System.Windows.Forms.Button();
            this.restartButton = new System.Windows.Forms.Button();
            this.closeButton = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.textBox1);
            this.flowLayoutPanel1.Controls.Add(this.textBox2);
            this.flowLayoutPanel1.Controls.Add(this.textBox3);
            this.flowLayoutPanel1.Controls.Add(this.textBox4);
            this.flowLayoutPanel1.Controls.Add(this.textBox5);
            this.flowLayoutPanel1.Controls.Add(this.textBox6);
            this.flowLayoutPanel1.Controls.Add(this.textBox7);
            this.flowLayoutPanel1.Controls.Add(this.textBox8);
            this.flowLayoutPanel1.Controls.Add(this.textBox9);
            this.flowLayoutPanel1.Controls.Add(this.textBox10);
            this.flowLayoutPanel1.Controls.Add(this.textBox11);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(535, 74);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox1.Enabled = false;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(42, 53);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox1.Visible = false;
            // 
            // textBox2
            // 
            this.textBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(51, 3);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(42, 53);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox2.Visible = false;
            // 
            // textBox3
            // 
            this.textBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(99, 3);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(42, 53);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox3.Visible = false;
            // 
            // textBox4
            // 
            this.textBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox4.Enabled = false;
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(147, 3);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(42, 53);
            this.textBox4.TabIndex = 3;
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox4.Visible = false;
            // 
            // textBox5
            // 
            this.textBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox5.Enabled = false;
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(195, 3);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(42, 53);
            this.textBox5.TabIndex = 4;
            this.textBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox5.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(243, 3);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(42, 53);
            this.textBox6.TabIndex = 5;
            this.textBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox6.Visible = false;
            // 
            // textBox7
            // 
            this.textBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox7.Enabled = false;
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(291, 3);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(42, 53);
            this.textBox7.TabIndex = 6;
            this.textBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox7.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox8.Enabled = false;
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(339, 3);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(42, 53);
            this.textBox8.TabIndex = 7;
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox8.Visible = false;
            // 
            // textBox9
            // 
            this.textBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox9.Enabled = false;
            this.textBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox9.Location = new System.Drawing.Point(387, 3);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(42, 53);
            this.textBox9.TabIndex = 8;
            this.textBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox9.Visible = false;
            // 
            // textBox10
            // 
            this.textBox10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox10.Enabled = false;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(435, 3);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(42, 53);
            this.textBox10.TabIndex = 9;
            this.textBox10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox10.Visible = false;
            // 
            // textBox11
            // 
            this.textBox11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox11.Enabled = false;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(483, 3);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(42, 53);
            this.textBox11.TabIndex = 10;
            this.textBox11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox11.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 10;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.Controls.Add(this.aButton, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.bButton, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cButton, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.dButton, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.eButton, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.fButton, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.gButton, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.hButton, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.iButton, 8, 0);
            this.tableLayoutPanel1.Controls.Add(this.jButton, 9, 0);
            this.tableLayoutPanel1.Controls.Add(this.kButton, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lButton, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.mButton, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.nButton, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.oButton, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.pButton, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.qButton, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.rButton, 7, 1);
            this.tableLayoutPanel1.Controls.Add(this.sButton, 8, 1);
            this.tableLayoutPanel1.Controls.Add(this.tButton, 9, 1);
            this.tableLayoutPanel1.Controls.Add(this.uButton, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.vButton, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.wButton, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.xButton, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.yButton, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.zButton, 7, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 102);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(535, 336);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // tButton
            // 
            this.tButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tButton.Enabled = false;
            this.tButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tButton.Location = new System.Drawing.Point(480, 115);
            this.tButton.Name = "tButton";
            this.tButton.Size = new System.Drawing.Size(52, 106);
            this.tButton.TabIndex = 19;
            this.tButton.Text = "T";
            this.tButton.UseVisualStyleBackColor = true;
            this.tButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // sButton
            // 
            this.sButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sButton.Enabled = false;
            this.sButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sButton.Location = new System.Drawing.Point(427, 115);
            this.sButton.Name = "sButton";
            this.sButton.Size = new System.Drawing.Size(47, 106);
            this.sButton.TabIndex = 18;
            this.sButton.Text = "S";
            this.sButton.UseVisualStyleBackColor = true;
            this.sButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // rButton
            // 
            this.rButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rButton.Enabled = false;
            this.rButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rButton.Location = new System.Drawing.Point(374, 115);
            this.rButton.Name = "rButton";
            this.rButton.Size = new System.Drawing.Size(47, 106);
            this.rButton.TabIndex = 17;
            this.rButton.Text = "R";
            this.rButton.UseVisualStyleBackColor = true;
            this.rButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // qButton
            // 
            this.qButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.qButton.Enabled = false;
            this.qButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.qButton.Location = new System.Drawing.Point(321, 115);
            this.qButton.Name = "qButton";
            this.qButton.Size = new System.Drawing.Size(47, 106);
            this.qButton.TabIndex = 16;
            this.qButton.Text = "Q";
            this.qButton.UseVisualStyleBackColor = true;
            this.qButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // pButton
            // 
            this.pButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pButton.Enabled = false;
            this.pButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pButton.Location = new System.Drawing.Point(268, 115);
            this.pButton.Name = "pButton";
            this.pButton.Size = new System.Drawing.Size(47, 106);
            this.pButton.TabIndex = 15;
            this.pButton.Text = "P";
            this.pButton.UseVisualStyleBackColor = true;
            this.pButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // oButton
            // 
            this.oButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oButton.Enabled = false;
            this.oButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.oButton.Location = new System.Drawing.Point(215, 115);
            this.oButton.Name = "oButton";
            this.oButton.Size = new System.Drawing.Size(47, 106);
            this.oButton.TabIndex = 14;
            this.oButton.Text = "O";
            this.oButton.UseVisualStyleBackColor = true;
            this.oButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // nButton
            // 
            this.nButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nButton.Enabled = false;
            this.nButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nButton.Location = new System.Drawing.Point(162, 115);
            this.nButton.Name = "nButton";
            this.nButton.Size = new System.Drawing.Size(47, 106);
            this.nButton.TabIndex = 13;
            this.nButton.Text = "N";
            this.nButton.UseVisualStyleBackColor = true;
            this.nButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // mButton
            // 
            this.mButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mButton.Enabled = false;
            this.mButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mButton.Location = new System.Drawing.Point(109, 115);
            this.mButton.Name = "mButton";
            this.mButton.Size = new System.Drawing.Size(47, 106);
            this.mButton.TabIndex = 12;
            this.mButton.Text = "M";
            this.mButton.UseVisualStyleBackColor = true;
            this.mButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // lButton
            // 
            this.lButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lButton.Enabled = false;
            this.lButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lButton.Location = new System.Drawing.Point(56, 115);
            this.lButton.Name = "lButton";
            this.lButton.Size = new System.Drawing.Size(47, 106);
            this.lButton.TabIndex = 11;
            this.lButton.Text = "L";
            this.lButton.UseVisualStyleBackColor = true;
            this.lButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // kButton
            // 
            this.kButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.kButton.Enabled = false;
            this.kButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.kButton.Location = new System.Drawing.Point(3, 115);
            this.kButton.Name = "kButton";
            this.kButton.Size = new System.Drawing.Size(47, 106);
            this.kButton.TabIndex = 10;
            this.kButton.Text = "K";
            this.kButton.UseVisualStyleBackColor = true;
            this.kButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // jButton
            // 
            this.jButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.jButton.Enabled = false;
            this.jButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.jButton.Location = new System.Drawing.Point(480, 3);
            this.jButton.Name = "jButton";
            this.jButton.Size = new System.Drawing.Size(52, 106);
            this.jButton.TabIndex = 9;
            this.jButton.Text = "J";
            this.jButton.UseVisualStyleBackColor = true;
            this.jButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // iButton
            // 
            this.iButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.iButton.Enabled = false;
            this.iButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iButton.Location = new System.Drawing.Point(427, 3);
            this.iButton.Name = "iButton";
            this.iButton.Size = new System.Drawing.Size(47, 106);
            this.iButton.TabIndex = 8;
            this.iButton.Text = "I";
            this.iButton.UseVisualStyleBackColor = true;
            this.iButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // hButton
            // 
            this.hButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.hButton.Enabled = false;
            this.hButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hButton.Location = new System.Drawing.Point(374, 3);
            this.hButton.Name = "hButton";
            this.hButton.Size = new System.Drawing.Size(47, 106);
            this.hButton.TabIndex = 7;
            this.hButton.Text = "H";
            this.hButton.UseVisualStyleBackColor = true;
            this.hButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // gButton
            // 
            this.gButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gButton.Enabled = false;
            this.gButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gButton.Location = new System.Drawing.Point(321, 3);
            this.gButton.Name = "gButton";
            this.gButton.Size = new System.Drawing.Size(47, 106);
            this.gButton.TabIndex = 6;
            this.gButton.Text = "G";
            this.gButton.UseVisualStyleBackColor = true;
            this.gButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // fButton
            // 
            this.fButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fButton.Enabled = false;
            this.fButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fButton.Location = new System.Drawing.Point(268, 3);
            this.fButton.Name = "fButton";
            this.fButton.Size = new System.Drawing.Size(47, 106);
            this.fButton.TabIndex = 5;
            this.fButton.Text = "F";
            this.fButton.UseVisualStyleBackColor = true;
            this.fButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // eButton
            // 
            this.eButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.eButton.Enabled = false;
            this.eButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eButton.Location = new System.Drawing.Point(215, 3);
            this.eButton.Name = "eButton";
            this.eButton.Size = new System.Drawing.Size(47, 106);
            this.eButton.TabIndex = 4;
            this.eButton.Text = "E";
            this.eButton.UseVisualStyleBackColor = true;
            this.eButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // dButton
            // 
            this.dButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dButton.Enabled = false;
            this.dButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dButton.Location = new System.Drawing.Point(162, 3);
            this.dButton.Name = "dButton";
            this.dButton.Size = new System.Drawing.Size(47, 106);
            this.dButton.TabIndex = 3;
            this.dButton.Text = "D";
            this.dButton.UseVisualStyleBackColor = true;
            this.dButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // cButton
            // 
            this.cButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cButton.Enabled = false;
            this.cButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cButton.Location = new System.Drawing.Point(109, 3);
            this.cButton.Name = "cButton";
            this.cButton.Size = new System.Drawing.Size(47, 106);
            this.cButton.TabIndex = 2;
            this.cButton.Text = "C";
            this.cButton.UseVisualStyleBackColor = true;
            this.cButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // bButton
            // 
            this.bButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bButton.Enabled = false;
            this.bButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bButton.Location = new System.Drawing.Point(56, 3);
            this.bButton.Name = "bButton";
            this.bButton.Size = new System.Drawing.Size(47, 106);
            this.bButton.TabIndex = 1;
            this.bButton.Text = "B";
            this.bButton.UseVisualStyleBackColor = true;
            this.bButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // zButton
            // 
            this.zButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.zButton.Enabled = false;
            this.zButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.zButton.Location = new System.Drawing.Point(374, 227);
            this.zButton.Name = "zButton";
            this.zButton.Size = new System.Drawing.Size(47, 106);
            this.zButton.TabIndex = 25;
            this.zButton.Text = "Z";
            this.zButton.UseVisualStyleBackColor = true;
            this.zButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // yButton
            // 
            this.yButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.yButton.Enabled = false;
            this.yButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yButton.Location = new System.Drawing.Point(321, 227);
            this.yButton.Name = "yButton";
            this.yButton.Size = new System.Drawing.Size(47, 106);
            this.yButton.TabIndex = 24;
            this.yButton.Text = "Y";
            this.yButton.UseVisualStyleBackColor = true;
            this.yButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // xButton
            // 
            this.xButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xButton.Enabled = false;
            this.xButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xButton.Location = new System.Drawing.Point(268, 227);
            this.xButton.Name = "xButton";
            this.xButton.Size = new System.Drawing.Size(47, 106);
            this.xButton.TabIndex = 23;
            this.xButton.Text = "X";
            this.xButton.UseVisualStyleBackColor = true;
            this.xButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // wButton
            // 
            this.wButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wButton.Enabled = false;
            this.wButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.wButton.Location = new System.Drawing.Point(215, 227);
            this.wButton.Name = "wButton";
            this.wButton.Size = new System.Drawing.Size(47, 106);
            this.wButton.TabIndex = 22;
            this.wButton.Text = "W";
            this.wButton.UseVisualStyleBackColor = true;
            this.wButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // vButton
            // 
            this.vButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.vButton.Enabled = false;
            this.vButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vButton.Location = new System.Drawing.Point(162, 227);
            this.vButton.Name = "vButton";
            this.vButton.Size = new System.Drawing.Size(47, 106);
            this.vButton.TabIndex = 21;
            this.vButton.Text = "V";
            this.vButton.UseVisualStyleBackColor = true;
            this.vButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // uButton
            // 
            this.uButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uButton.Enabled = false;
            this.uButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uButton.Location = new System.Drawing.Point(109, 227);
            this.uButton.Name = "uButton";
            this.uButton.Size = new System.Drawing.Size(47, 106);
            this.uButton.TabIndex = 20;
            this.uButton.Text = "U";
            this.uButton.UseVisualStyleBackColor = true;
            this.uButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // aButton
            // 
            this.aButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aButton.Enabled = false;
            this.aButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aButton.Location = new System.Drawing.Point(3, 3);
            this.aButton.Name = "aButton";
            this.aButton.Size = new System.Drawing.Size(47, 106);
            this.aButton.TabIndex = 0;
            this.aButton.Text = "A";
            this.aButton.UseVisualStyleBackColor = true;
            this.aButton.Click += new System.EventHandler(this.letterButton_Click);
            // 
            // startButton
            // 
            this.startButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startButton.Location = new System.Drawing.Point(638, 280);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 3;
            this.startButton.Text = "Start";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // restartButton
            // 
            this.restartButton.Enabled = false;
            this.restartButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.restartButton.Location = new System.Drawing.Point(638, 319);
            this.restartButton.Name = "restartButton";
            this.restartButton.Size = new System.Drawing.Size(75, 23);
            this.restartButton.TabIndex = 4;
            this.restartButton.Text = "Restart";
            this.restartButton.UseVisualStyleBackColor = true;
            this.restartButton.Click += new System.EventHandler(this.restartButton_Click);
            // 
            // closeButton
            // 
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.Location = new System.Drawing.Point(638, 358);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(75, 23);
            this.closeButton.TabIndex = 5;
            this.closeButton.Text = "Close";
            this.closeButton.UseVisualStyleBackColor = true;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Hangman.Properties.Resources.Hangman;
            this.pictureBox1.Location = new System.Drawing.Point(560, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(228, 212);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.closeButton);
            this.Controls.Add(this.restartButton);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Hangman";
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button tButton;
        private System.Windows.Forms.Button sButton;
        private System.Windows.Forms.Button rButton;
        private System.Windows.Forms.Button qButton;
        private System.Windows.Forms.Button pButton;
        private System.Windows.Forms.Button oButton;
        private System.Windows.Forms.Button nButton;
        private System.Windows.Forms.Button mButton;
        private System.Windows.Forms.Button lButton;
        private System.Windows.Forms.Button kButton;
        private System.Windows.Forms.Button jButton;
        private System.Windows.Forms.Button iButton;
        private System.Windows.Forms.Button hButton;
        private System.Windows.Forms.Button gButton;
        private System.Windows.Forms.Button fButton;
        private System.Windows.Forms.Button eButton;
        private System.Windows.Forms.Button dButton;
        private System.Windows.Forms.Button cButton;
        private System.Windows.Forms.Button bButton;
        private System.Windows.Forms.Button zButton;
        private System.Windows.Forms.Button yButton;
        private System.Windows.Forms.Button xButton;
        private System.Windows.Forms.Button wButton;
        private System.Windows.Forms.Button vButton;
        private System.Windows.Forms.Button uButton;
        private System.Windows.Forms.Button aButton;
        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.Button restartButton;
        private System.Windows.Forms.Button closeButton;

        private string chosenWord;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;

        private List<char> guessedLetters;
        private Dictionary<object, char> letterButtons;
        private int wrongGuesses;
    }
}

