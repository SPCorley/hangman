Hangman

A simple implementation of the game hangman using C# with Windows Forms in Visual Studio.

Sample video and photo:

![Sample Video](img/hangman.m4v)

![alt text](img/hangman.png "Hangman")
